/*
 * Copyright (c) 2016 National Tsing Hua University.
 * \brief The Hexagon OpenVX Kernel Implementation
 * \author Tzu-Hsiang Lin <thlin@pllab.cs.nthu.edu.tw>
 */

#include <vx_hexagon_interface.h>
#include <vx_hexagon.h>
#include "hex_common.h"

vx_status vxMedian3x3(vx_node node, vx_image src, vx_image dst, vx_border_t *borders)
{
	return VX_ERROR_NOT_IMPLEMENTED;
	//return commonOneIOneO(src, dst, );
}

vx_status vxBox3x3(vx_node node, vx_image src, vx_image dst, vx_border_t *bordermode)
{
	return commonOneIOneO(node, src, dst, vx_hexagon_fcvBoxFilter3x3u8);
}

vx_status vxGaussian3x3(vx_node node, vx_image src, vx_image dst, vx_border_t *bordermode)
{
	return commonOneIOneO(node, src, dst, vx_hexagon_fcvFilterGaussian3x3u8_v2);
}
