/*
 * Copyright (c) 2016 National Tsing Hua University.
 * \brief The Hexagon OpenVX Kernel Implementation
 * \author Tzu-Hsiang Lin <thlin@pllab.cs.nthu.edu.tw>
 */

#include <iostream>
#include "vx_hexagon_interface.h"
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
using namespace std;
using namespace cv;

int main(int argc, char* argv[])
{
    uint32_t srcWidth = 640;
    uint32_t srcHeight = 480;

    cout <<  "vx_hexagon_interface_init()..." << std::endl;
	int ret = vx_hexagon_interface_init();
    if(ret < 0)
    {
    	printf("vx_hexagon_interface_init fail ret=%d\n", ret);
    	return -1;
    }

    cout <<  "Opening lena.jpg..." << std::endl;
    Mat im_resized, im_rgb = imread("lena.jpg");
    Mat im_result(srcHeight, srcWidth, CV_8UC1);
    if(!im_rgb.data )                              // Check for invalid input
    {
        cout <<  "Could not open or find the image" << std::endl;
        return -1;
    }
    cout <<  "Resizing..." << std::endl;
    resize(im_rgb, im_resized, Size(srcWidth, srcHeight));
	cvtColor(im_resized, im_resized, CV_BGR2GRAY);

	cout <<  "vx_set_debug_zone_from_env()..." << std::endl;
	vx_set_debug_zone_from_env();
	
    cout <<  "vx_hexagon_process()..." << std::endl;
    ret = vx_hexagon_process(srcWidth, srcHeight, im_resized.data, im_result.data);
    if(ret < 0)
    {
        printf("vx_hexagon_process fail ret=%d\n", ret);
    }

    cout <<  "vx_hexagon_interface_deinit()..." << std::endl;
    ret = vx_hexagon_interface_deinit();
    if(ret < 0)
    {
    	printf("vx_hexagon_interface_deinit fail ret=%d\n", ret);
    }

    cout <<  "Writing result to result.jpg..." << std::endl;
    imwrite("result.jpg", im_result);

    return 0;
}
