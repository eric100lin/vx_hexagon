/*
 * Copyright (c) 2016 National Tsing Hua University.
 * \brief The Hexagon OpenVX Kernel Implementation
 * \author Tzu-Hsiang Lin <thlin@pllab.cs.nthu.edu.tw>
 */

#include <vx_hexagon_interface.h>
#include <vx_hexagon.h>
#include "hex_common.h"

vx_status vxAnd(vx_node node, vx_image in1, vx_image in2, vx_image output)
{
	return commonTwoIOneO(node, in1, in2, output, vx_hexagon_fcvBitwiseAndu8);
}

vx_status vxXor(vx_node node, vx_image in1, vx_image in2, vx_image output)
{
	return commonTwoIOneO(node, in1, in2, output, vx_hexagon_fcvBitwiseXoru8);
}

vx_status vxNot(vx_node node, vx_image input, vx_image output)
{
	return commonOneIOneO(node, input, output, vx_hexagon_fcvBitwiseNotu8);
}
