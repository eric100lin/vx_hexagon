/*
 * Copyright (c) 2016 National Tsing Hua University.
 * \brief The Hexagon OpenVX Kernel Implementation
 * \author Tzu-Hsiang Lin <thlin@pllab.cs.nthu.edu.tw>
 */
 
#ifndef _VX_HEXAGON_INTERFACE_H_
#define _VX_HEXAGON_INTERFACE_H_

#include <hexagon_model.h>
#include <vx_debug.h>
#define INIT_ION_RPCMEM { 0, NULL, NULL }

#ifdef __cplusplus
extern "C" {
#endif 

int vx_hexagon_interface_init();

int vx_hexagon_interface_deinit();

typedef struct ion_rpcmem
{
	vx_size len;
	void *ptr;
	vx_image vxImg;
} ion_rpcmem_t;

void releae_rpcmem(ion_rpcmem_t rpcmem);

vx_status vx_image_to_rpcmem(vx_image vxImg, uint32_t *width, uint32_t *height, ion_rpcmem_t *rpcmem);

vx_status allocate_vx_image_rpcmem(vx_image vxImg, ion_rpcmem_t *rpcmem);

vx_status rpcmem_to_vx_image(vx_image vxImg, ion_rpcmem_t *rpcmem);

int vx_hexagon_process(uint32_t srcWidth, uint32_t srcHeight, uint8_t *ptrSrcImage, uint8_t *ptrDstImage);

#ifdef __cplusplus
}
#endif 

#endif
