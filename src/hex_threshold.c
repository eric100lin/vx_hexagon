/*
 * Copyright (c) 2016 National Tsing Hua University.
 * \brief The Hexagon OpenVX Kernel Implementation
 * \author Tzu-Hsiang Lin <thlin@pllab.cs.nthu.edu.tw>
 */

#include <vx_hexagon_interface.h>
#include <vx_hexagon.h>
#include "hex_common.h"

vx_status vxThreshold(vx_node node, vx_image src, vx_threshold vxThreshold, vx_image dst)
{
	vx_status status = VX_SUCCESS;
	ion_rpcmem_t src_rpcmem = INIT_ION_RPCMEM;
	ion_rpcmem_t dst_rpcmem = INIT_ION_RPCMEM;
	int remote_ret;
	uint32_t width, height, srcStride, dstStride;
	uint32_t threshold;
	
	VX_PRINT(VX_ZONE_TAR_HEXAGON, "vxThreshold() start~~\n");
	
	vxQueryThreshold(vxThreshold, VX_THRESHOLD_THRESHOLD_VALUE, &threshold, sizeof(vx_int32));
	threshold = (threshold < 255) ? threshold+1 : threshold;	//FastCV is >= threshold will get 255
	VX_PRINT(VX_ZONE_TAR_HEXAGON, "vxQueryThreshold returned threshold %d\n", threshold);
	
	status = vx_image_to_rpcmem(src, &width, &height, &src_rpcmem);
	if(status != VX_SUCCESS)	goto error_read_image;
	
	status = allocate_vx_image_rpcmem(dst, &dst_rpcmem);
	if(status != VX_SUCCESS)	goto error_allocate_dst_image;

	srcStride = width;	dstStride = width;
	vxNodeStartTimeCapture(node);				//Measure DSP computing time START
	remote_ret = vx_hexagon_fcvFilterThresholdu8_v2(
	      (uint8 *)src_rpcmem.ptr, src_rpcmem.len, 
		  width, height, srcStride, 
		  (uint8 *)dst_rpcmem.ptr, dst_rpcmem.len, 
		  dstStride, threshold);
	vxNodeStopTimeCapture(node);				//Measure DSP computing time STOP

	if(remote_ret != 0) 
	{
		status = VX_FAILURE;
		VX_PRINT(VX_ZONE_TAR_HEXAGON, "vx_hexagon_fcvFilterThresholdu8_v2 returned %d\n", remote_ret);
		goto error_processing;
	}
	
error_processing:	
	status = rpcmem_to_vx_image(dst, &dst_rpcmem);
	if(status != VX_SUCCESS)	goto error_write_image;
error_write_image:
	releae_rpcmem(dst_rpcmem);
error_allocate_dst_image:
	releae_rpcmem(src_rpcmem);
error_read_image:
	return status;
}
