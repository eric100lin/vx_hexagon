/*
 * Copyright (c) 2016 National Tsing Hua University.
 * \brief The Hexagon OpenVX Kernel Implementation
 * \author Tzu-Hsiang Lin <thlin@pllab.cs.nthu.edu.tw>
 */

#include <vx_hexagon_interface.h>
#include <vx_hexagon.h>
#include "hex_common.h"

vx_status commonOneIOneO(vx_node node, vx_image src, vx_image dst, 
	AEEResult (*remote_method)(const uint8* src, int srcLen, uint32 width, uint32 height, 
							   uint32 srcStride, uint8* dst, int dstLen, uint32 dstStride))
{
	vx_status status = VX_SUCCESS;
	ion_rpcmem_t src_rpcmem = INIT_ION_RPCMEM;
	ion_rpcmem_t dst_rpcmem = INIT_ION_RPCMEM;
	int remote_ret;
	uint32_t width, height, srcStride, dstStride;
	
	status = vx_image_to_rpcmem(src, &width, &height, &src_rpcmem);
	if(status != VX_SUCCESS)	goto error_read_image;
	
	status = allocate_vx_image_rpcmem(dst, &dst_rpcmem);
	if(status != VX_SUCCESS)	goto error_allocate_dst_image;

	srcStride = width;	dstStride = width;
	vxNodeStartTimeCapture(node);				//Measure DSP computing time START
	remote_ret = remote_method((uint8 *)src_rpcmem.ptr, src_rpcmem.len, 
							   width, height, srcStride, 
							   (uint8 *)dst_rpcmem.ptr, dst_rpcmem.len, 
							   dstStride);
	vxNodeStopTimeCapture(node);				//Measure DSP computing time STOP

	if(remote_ret != 0) 
	{
		status = VX_FAILURE;
		goto error_processing;
	}
	
	status = rpcmem_to_vx_image(dst, &dst_rpcmem);
	if(status != VX_SUCCESS)	goto error_write_image;

error_write_image:
error_processing:
	releae_rpcmem(dst_rpcmem);
error_allocate_dst_image:
	releae_rpcmem(src_rpcmem);
error_read_image:
	return status;
}

vx_status commonTwoIOneO(vx_node node, vx_image in1, vx_image in2, vx_image output,
	AEEResult (*remote_method)(const uint8* src1, int src1Len, uint32 width, uint32 height, 
							   uint32 src1Stride, 
							   const uint8* src2, int src2Len, uint32 src2Stride, 
							   uint8* dst, int dstLen, uint32 dstStride))
{
	vx_status status = VX_SUCCESS;
	ion_rpcmem_t src1_rpcmem = INIT_ION_RPCMEM;
	ion_rpcmem_t src2_rpcmem = INIT_ION_RPCMEM;
	ion_rpcmem_t dst_rpcmem = INIT_ION_RPCMEM;
	int remote_ret;
	uint32_t width, height, srcStride, dstStride;
	
	status = vx_image_to_rpcmem(in1, &width, &height, &src1_rpcmem);
	if(status != VX_SUCCESS)	goto error_read_image1;
	
	status = vx_image_to_rpcmem(in2, &width, &height, &src2_rpcmem);
	if(status != VX_SUCCESS)	goto error_read_image2;
	
	status = allocate_vx_image_rpcmem(output, &dst_rpcmem);
	if(status != VX_SUCCESS)	goto error_allocate_dst_image;
	
	srcStride = width;	dstStride = width;
	vxNodeStartTimeCapture(node);				//Measure DSP computing time START
	remote_ret = remote_method((uint8 *)src1_rpcmem.ptr, src1_rpcmem.len, 
							   width, height, srcStride, 
							   (uint8 *)src2_rpcmem.ptr, src2_rpcmem.len, 
							   srcStride, 
							   (uint8 *)dst_rpcmem.ptr, dst_rpcmem.len, 
							   dstStride);
	vxNodeStopTimeCapture(node);				//Measure DSP computing time STOP
	if(remote_ret != 0) 
	{
		status = VX_FAILURE;
		goto error_processing;
	}
	
	status = rpcmem_to_vx_image(output, &dst_rpcmem);
	if(status != VX_SUCCESS)	goto error_write_image;
	
error_write_image:
error_processing:
	releae_rpcmem(dst_rpcmem);
error_allocate_dst_image:
	releae_rpcmem(src2_rpcmem);
error_read_image2:
	releae_rpcmem(src1_rpcmem);
error_read_image1:
	return status;
}