#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "verify.h"
#include <stdio.h>
#include <dspCV.h>
#include <vx_hexagon.h>
#include "vx_hexagon_interface.h"

#ifdef __hexagon__     // some defs so app can build for Android or Hexagon simulation
#include "hexagon_sim_timer.h"
#define GetTime hexagon_sim_read_pcycles        // For Hexagon sim, use PCycles for profiling
#define rpcmem_init()
#define rpcmem_deinit()
#define rpcmem_alloc(a, b, c) malloc((c))
#define rpcmem_free(a) free((a))
#define LOOPS 1

int adsp_pls_add_lookup(uint32 type, uint32 key, int size, int (*ctor)(void* ctx, void* data), void* ctx, void (*dtor)(void*), void** ppo)
{
   return 0;
}

int apps_mem_heap_init(int id, int flags, int rflags, int size) {
   return 0;
}

uint32_t HAP_get_chip_family_id(void)
{
   return 0;
}

void* apps_mem_heap_malloc(int size) { return malloc(size); }

void apps_mem_heap_free(void* po) { return free(po); }

#else
#include "rpcmem.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/time.h>
#define LOOPS 100          // reduce the profiling impact of dynamic loading. Load once, and run FastRPC+processing many times.

unsigned long long GetTime( void )
{
	struct timeval tv;
	struct timezone tz;

	gettimeofday(&tv, &tz);

	return tv.tv_sec * 1000000ULL + tv.tv_usec;
}

#endif 

#ifndef __hexagon__    
    uint32_t heapId = 22;
#endif

extern void *vxGetIONmemoryPtr(vx_image image);
extern void vxSetIONmemoryPtr(vx_image image, void *ptr);
extern vx_bool vxCanReleaseIONmemory(vx_image image);
extern vx_bool vxIsLocalOptimized(vx_image image);
extern vx_bool vxIsTransferBackNeeded(vx_image image);

int vx_hexagon_interface_init()
{
	int nErr = 0;
	dspCV_Attribute attrib = {CLOCK_PRESET_MODE, MAX_PERFORMANCE_MODE};
	
	rpcmem_init();
    
#ifndef __hexagon__    
    uint8_t *dummy = (uint8_t*)rpcmem_alloc(heapId, 0, 2048 * 2048);
    if (!dummy) heapId = 25;
    rpcmem_free(dummy);
#endif
	
	// call dspCV_init to boost DSP clocks and initialize worker thread pool
	nErr = dspCV_initQ6_with_attributes(&attrib, 1);
    if (nErr)
	{
		printf("error in dspCV_initQ6: %d\n", nErr);
		rpcmem_deinit();
        return -1;
	}
	
	return 0;
}

int vx_hexagon_interface_deinit()
{
	int nErr = 0;
	rpcmem_deinit();
	
	nErr = dspCV_deinitQ6();
	if(nErr)
	{
		printf("error in deinitQ6: %d\n", nErr);
	}
	return nErr;
}

vx_status vxHexagonInit()
{
	vx_status status = VX_SUCCESS;
	int ret = vx_hexagon_interface_init();
	if(ret!=0)	status = VX_FAILURE;
	
	VX_PRINT(VX_ZONE_TAR_HEXAGON, "vx_hexagon_interface_init returned %d\n", status);
	return status;
}

vx_status vxHexagonDeInit()
{
	vx_status status = VX_SUCCESS;
	int ret = vx_hexagon_interface_deinit();
	if(ret!=0)	status = VX_FAILURE;
	
	VX_PRINT(VX_ZONE_TAR_HEXAGON, "vx_hexagon_interface_deinit returned %d\n", status);
	return status;
}

static vx_status allocate_rpcmem(ion_rpcmem_t *rpcmem)
{
	vx_status status = VX_SUCCESS;
	rpcmem->ptr = rpcmem_alloc(heapId, 0, rpcmem->len);
	if(rpcmem->ptr == 0)
		status = VX_ERROR_NO_RESOURCES;
	
	VX_PRINT(VX_ZONE_TAR_HEXAGON, "rpcmem_alloc(heapId=%d len:%d ptr:%p) %s\n", 
		heapId, rpcmem->len, rpcmem->ptr, (status==VX_SUCCESS) ? "success" : "fail");
	VX_PRINT(VX_ZONE_TAR_HEXAGON, "allocate_rpcmem returned %d\n", status);
	return status;
}

static vx_status get_vx_image_size(vx_image vxImg, vx_size *vxImgSize)
{
	vx_status status = VX_SUCCESS;
	vx_rectangle_t rect;
	vx_size n_plane, vxImgPlanes = 0ul;

	status = vxGetValidRegionImage(vxImg, &rect);
	if(status != VX_SUCCESS) return status;
	
	status = vxQueryImage(vxImg, VX_IMAGE_PLANES, &vxImgPlanes, sizeof(vxImgPlanes));
	if(status != VX_SUCCESS) return status;
	
	*vxImgSize = 0ul;
	for(n_plane=0; n_plane<vxImgPlanes; n_plane++)
		(*vxImgSize) += vxComputeImagePatchSize(vxImg, &rect, n_plane);
	if(*vxImgSize == 0)	return VX_ERROR_INVALID_VALUE;
	
	VX_PRINT(VX_ZONE_TAR_HEXAGON, "vxImgSize %d\n", *vxImgSize);
	return status;
}

void releae_rpcmem(ion_rpcmem_t rpcmem)
{
	if (!rpcmem.ptr) 
		return;
	if (rpcmem.vxImg != NULL &&
	    vxCanReleaseIONmemory(rpcmem.vxImg) == vx_false_e)
		return;
	
	rpcmem_free(rpcmem.ptr);
	VX_PRINT(VX_ZONE_TAR_HEXAGON, "rpcmem_free len:%d ptr:%p\n", rpcmem.len, rpcmem.ptr);
}

vx_status vx_image_to_rpcmem(vx_image vxImg, uint32_t *width, uint32_t *height, ion_rpcmem_t *rpcmem)
{
	vx_status status = VX_SUCCESS;
	vx_rectangle_t rect;
	vx_imagepatch_addressing_t vxImg_addr;
	void *vxImg_base = NULL;
	
	status = vxGetValidRegionImage(vxImg, &rect);
	if(status != VX_SUCCESS) return status;
	
	status = vxAccessImagePatch(vxImg, &rect, 0, &vxImg_addr, (void **)&vxImg_base, VX_READ_ONLY);
	if(status != VX_SUCCESS) return status;
	
    *width  = vxImg_addr.dim_x;
    *height = vxImg_addr.dim_y;
	
	if(vxIsLocalOptimized(vxImg) == vx_true_e)
	{
		status = get_vx_image_size(vxImg, &rpcmem->len);
		if(status != VX_SUCCESS) return status;
		
		rpcmem->ptr = vxGetIONmemoryPtr(vxImg);
		rpcmem->vxImg = vxImg;
	}
	else
	{
		status = allocate_vx_image_rpcmem(vxImg, rpcmem);
		if(status != VX_SUCCESS) return status;
		
		memcpy(rpcmem->ptr, vxImg_base, rpcmem->len);
	}
	
	status = vxCommitImagePatch(vxImg, NULL, 0, &vxImg_addr, vxImg_base);
	
	VX_PRINT(VX_ZONE_TAR_HEXAGON, "vx_image -> rpcmem returned %d\n", status);
	return status;
}

vx_status allocate_vx_image_rpcmem(vx_image vxImg, ion_rpcmem_t *rpcmem)
{
	vx_status status = VX_SUCCESS;
	
	status = get_vx_image_size(vxImg, &rpcmem->len);
	if(status != VX_SUCCESS) return status;
	
	return allocate_rpcmem(rpcmem);
}

vx_status rpcmem_to_vx_image(vx_image vxImg, ion_rpcmem_t *rpcmem)
{
	vx_status status = VX_SUCCESS;
	vx_rectangle_t rect;
	vx_imagepatch_addressing_t vxImg_addr;
	void *vxImg_base = NULL;
	
	status = vxGetValidRegionImage(vxImg, &rect);
	if(status != VX_SUCCESS) return status;
	
	if(vxIsLocalOptimized(vxImg) == vx_true_e)
	{
		vxSetIONmemoryPtr(vxImg, rpcmem->ptr);
		rpcmem->vxImg = vxImg;
	}
	
	if(vxIsTransferBackNeeded(vxImg) == vx_true_e)
	{
		status = vxAccessImagePatch(vxImg, &rect, 0, &vxImg_addr, (void **)&vxImg_base, VX_WRITE_ONLY);
		if(status != VX_SUCCESS) return status;
		
		memcpy(vxImg_base, rpcmem->ptr, rpcmem->len);
		
		status = vxCommitImagePatch(vxImg, &rect, 0, &vxImg_addr, vxImg_base);
	}
	
	VX_PRINT(VX_ZONE_TAR_HEXAGON, "rpcmem -> vx_image returned %d\n", status);
	return status;
}

int vx_hexagon_process(uint32_t srcWidth, uint32_t srcHeight, uint8_t *ptrSrcImage, uint8_t *ptrDstImage)
{
	int remote_ret;
	vx_status status = VX_SUCCESS;
	ion_rpcmem_t src_rpcmem = INIT_ION_RPCMEM;
	ion_rpcmem_t dst_rpcmem = INIT_ION_RPCMEM;
	uint32_t width=srcWidth, height=srcHeight, srcStride=srcWidth, dstStride=srcWidth;
	
	src_rpcmem.len = dst_rpcmem.len = srcWidth*srcHeight;
	status = allocate_rpcmem(&src_rpcmem);
	if(status != VX_SUCCESS) return -1;
	
	status = allocate_rpcmem(&dst_rpcmem);
	if(status != VX_SUCCESS) return -2;
	
	memcpy(src_rpcmem.ptr, ptrSrcImage, src_rpcmem.len);
	
	remote_ret = vx_hexagon_fcvBitwiseNotu8((uint8 *)src_rpcmem.ptr, src_rpcmem.len, 
											width, height, srcStride, 
											(uint8 *)dst_rpcmem.ptr, dst_rpcmem.len, 
											dstStride);
	VX_PRINT(VX_ZONE_TAR_HEXAGON, 
		"remote_ret: %d\n"
		"vx_hexagon_fcvBitwiseNotu8\n"
		"\tsrc_rpcmem.ptr: %p, src_rpcmem.len: %d\n"
		"\twidth: %d, height: %d, srcStride: %d\n"
		"\tdst_rpcmem.ptr: %p, dst_rpcmem.len: %d\n"
		"\tdstStride: %d\n", 
		remote_ret,
		src_rpcmem.ptr, src_rpcmem.len,
		width, height, srcStride, 
		dst_rpcmem.ptr, dst_rpcmem.len, 
		dstStride);

	if(remote_ret != 0) 
	{
		status = VX_FAILURE;
	}
	
	memcpy(ptrDstImage, dst_rpcmem.ptr, dst_rpcmem.len);
	
	releae_rpcmem(dst_rpcmem);
	releae_rpcmem(src_rpcmem);
	return status;
}
