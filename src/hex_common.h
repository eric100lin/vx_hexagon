/*
 * Copyright (c) 2016 National Tsing Hua University.
 * \brief The Hexagon OpenVX Kernel Implementation
 * \author Tzu-Hsiang Lin <thlin@pllab.cs.nthu.edu.tw>
 */

#ifndef _HEX_COMMON_H_
#define _HEX_COMMON_H_

#ifdef __cplusplus
extern "C" {
#endif 

extern void vxNodeStartTimeCapture(vx_node node);
extern void vxNodeStopTimeCapture(vx_node node);

vx_status commonOneIOneO(vx_node node, vx_image src, vx_image dst, 
	AEEResult (*remote_method)(const uint8* src, int srcLen, uint32 width, uint32 height, 
							   uint32 srcStride, uint8* dst, int dstLen, uint32 dstStride));

vx_status commonTwoIOneO(vx_node node, vx_image in1, vx_image in2, vx_image output,
	AEEResult (*remote_method)(const uint8* src1, int src1Len, uint32 width, uint32 height, 
							   uint32 src1Stride, 
							   const uint8* src2, int src2Len, uint32 src2Stride, 
							   uint8* dst, int dstLen, uint32 dstStride));

#ifdef __cplusplus
}
#endif 

#endif