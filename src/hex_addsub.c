/*
 * Copyright (c) 2016 National Tsing Hua University.
 * \brief The Hexagon OpenVX Kernel Implementation
 * \author Tzu-Hsiang Lin <thlin@pllab.cs.nthu.edu.tw>
 */

#include <vx_hexagon_interface.h>
#include <vx_hexagon.h>
#include "hex_common.h"

vx_status vxAddition(vx_node node, vx_image in1, vx_image in2, vx_scalar policy_param, vx_image output)
{
	vx_enum overflow_policy = -1;
	vxCopyScalar(policy_param, &overflow_policy, VX_READ_ONLY, VX_MEMORY_TYPE_HOST);
	
	if (overflow_policy == VX_CONVERT_POLICY_SATURATE)
		return commonTwoIOneO(node, in1, in2, output, vx_hexagon_fcvAddu8_SATURATE);
	else
		return commonTwoIOneO(node, in1, in2, output, vx_hexagon_fcvAddu8_WRAP);
}

vx_status vxSubtraction(vx_node node, vx_image in1, vx_image in2, vx_scalar policy_param, vx_image output)
{
	vx_enum overflow_policy = -1;
	vxCopyScalar(policy_param, &overflow_policy, VX_READ_ONLY, VX_MEMORY_TYPE_HOST);
	
	if (overflow_policy == VX_CONVERT_POLICY_SATURATE)
		return commonTwoIOneO(node, in1, in2, output, vx_hexagon_fcvSubtractu8_SATURATE);
	else
		return commonTwoIOneO(node, in1, in2, output, vx_hexagon_fcvSubtractu8_WRAP);
}
