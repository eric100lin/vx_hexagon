/*
 * Copyright (c) 2016 National Tsing Hua University.
 * \brief The Hexagon OpenVX Kernel Implementation
 * \author Tzu-Hsiang Lin <thlin@pllab.cs.nthu.edu.tw>
 */

#include <stdio.h>
#include <string.h>
#include "HAP_farf.h"
#include "fastcv.h"
#include "vx_hexagon.h"

/*===========================================================================
    REMOTED FUNCTION
===========================================================================*/
/*-------------------------------begin corner detection app functions---------------------------------------------------------------------*/

int vx_hexagon_filterGaussianAndCornerFastQ(const uint8* src,
		int srcLen, uint32 srcWidth, uint32 srcHeight, uint8* dst, int dstLen, int32 blurBorder,
		uint32 srcStride, int32 barrier, uint32 border, uint32* xy, int xylen, uint32 nCornersMax,
		uint32* nCorners,uint32* renderBuffer, int renderBufferLen)
{
	// Perform color conversion from YUV to RGB. Output renderBuffer is the original 
    // buffer after conversion to RGB, upon which the caller of this function must draw 
    // the corners detected in the desired manner. 
	fcvColorYUV420toRGB565u8(src, srcWidth, srcHeight, renderBuffer);

	// Gaussian blur the image and then detect corners using Fast9 detection algorithm.
    // Return the list of corners for application to draw upon the rendered image.
	fcvFilterGaussian3x3u8(src, srcWidth, srcHeight, dst, blurBorder);
    fcvCornerFast9u8(dst, srcWidth, srcHeight, srcStride, barrier, border, xy, nCornersMax, nCorners);

	return 0;
}

int vx_hexagon_YUV420toRGB565(const uint8* src, int srcLen, 
		uint32 srcWidth, uint32 srcHeight, uint32 *renderBuffer, int renderBufferLen)
{
	HAP_setFARFRuntimeLoggingParams(0x1f, NULL, 0);
	FARF(HIGH,"vx_hexagon_YUV420toRGB565");
	FARF(HIGH,"srcWidth = %d", srcWidth);
	FARF(HIGH,"srcHeight = %d", srcHeight);
	
	fcvColorYUV420toRGB565u8(src, srcWidth, srcHeight, renderBuffer);
	return 0;
}

int vx_hexagon_RGB565ToRGB888(const uint8* src, int srcLen, 
		uint32 srcWidth, uint32 srcHeight, uint8 *renderBuffer, int renderBufferLen)
{
	FARF(HIGH,"vx_hexagon_RGB565ToRGB888");
	uint32_t srcStride = 0;	//If left at 0, srcStride is default to srcWidth * 2
	uint32_t dstStride = 0;	//If left at 0, dstStride is default to srcWidth * 3
	
	fcvColorRGB565ToRGB888u8(src, srcWidth, srcHeight, srcStride, renderBuffer, dstStride);
	return 0;
}

int vx_hexagon_fcvColorVxRGBtoVxRGBX(const uint8* src, int srcLen, 
		uint32 srcWidth, uint32 srcHeight, uint8 *renderBuffer, int renderBufferLen)
{
	uint32_t srcStride = srcWidth * 3;	//If left at 0, srcStride is default to srcWidth * 3
	uint32_t dstStride = srcWidth * 4;	//If left at 0, dstStride is default to srcWidth * 4
	HAP_setFARFRuntimeLoggingParams(0x1f, NULL, 0);
	FARF(HIGH,"vx_hexagon_fcvColorVX_RGBtoVX_RGBX");
	FARF(HIGH,"srcWidth = %d", srcWidth);
	FARF(HIGH,"srcHeight = %d", srcHeight);
	FARF(HIGH,"srcStride = %d", srcStride);
	FARF(HIGH,"dstStride = %d", dstStride);

	// Perform color conversion from RGB888 to RGBA8888. 
	fcvColorRGB888ToRGBA8888u8(src, srcWidth, srcHeight, srcStride, renderBuffer, dstStride);

	return 0;
}

int vx_hexagon_fcvBitwiseNotu8
	(const uint8* src, int srcLen,
	 uint32_t          width,
	 uint32_t          height,
	 uint32_t          srcStride,
	 uint8 *dst,       int dstLen,
	 uint32_t          dstStride)
{
	return fcvBitwiseNotu8(src, width, height, srcStride, dst, dstStride);
}

int vx_hexagon_fcvBitwiseAndu8
	(const uint8* src1, int src1Len,
     uint32_t           width,
     uint32_t           height,
     uint32_t           src1Stride,
     const uint8* src2, int src2Len,
     uint32_t           src2Stride,
     uint8 *dst,        int dstLen,
     uint32_t           dstStride )
{
	return fcvBitwiseAndu8(src1, width, height, src1Stride, src2, src2Stride, dst, dstStride);
}

int vx_hexagon_fcvBitwiseXoru8
	(const uint8* src1, int src1Len,
     uint32_t           width,
     uint32_t           height,
     uint32_t           src1Stride,
     const uint8* src2, int src2Len,
     uint32_t           src2Stride,
     uint8 *dst,        int dstLen,
     uint32_t           dstStride )
{
	return fcvBitwiseXoru8(src1, width, height, src1Stride, src2, src2Stride, dst, dstStride);
}

int vx_hexagon_fcvBoxFilter3x3u8
	(const uint8* src, int srcLen,
	 uint32_t          srcWidth,
	 uint32_t          srcHeight,
	 uint32_t          srcStride,
	 uint8 *dst,       int dstLen,
	 uint32_t          dstStride)
{
	fcvBoxFilter3x3u8(src, srcWidth, srcHeight, srcStride, dst, dstStride);
	return 0;
}

int vx_hexagon_fcvFilterGaussian3x3u8_v2
	(const uint8* src, int srcLen,
	 uint32_t          srcWidth,
	 uint32_t          srcHeight,
	 uint32_t          srcStride,
	 uint8 *dst,       int dstLen,
	 uint32_t          dstStride)
{
	//If set blurBorder to 0, borders up to half-kernel width are ignored
	fcvFilterGaussian3x3u8_v2(src, srcWidth, srcHeight, srcStride, dst, dstStride,0);
	return 0;
}

int vx_hexagon_fcvAddu8_WRAP
	(const uint8_t *		     src1, int src1Len,
	 uint32_t                    width,			
	 uint32_t                    height,
	 uint32_t                    src1Stride,
	 const uint8_t * __restrict  src2, int src2Len,
	 uint32_t                    src2Stride,
	 uint8_t *                   dst,  int dstLen,
	 uint32_t                    dstStride )
{
	return fcvAddu8(src1, width, height, src1Stride, src2, src2Stride, FASTCV_CONVERT_POLICY_WRAP, dst, dstStride);
}

int vx_hexagon_fcvAddu8_SATURATE
	(const uint8_t *		     src1, int src1Len,
	 uint32_t                    width,			
	 uint32_t                    height,
	 uint32_t                    src1Stride,
	 const uint8_t * __restrict  src2, int src2Len,
	 uint32_t                    src2Stride,
	 uint8_t *                   dst,  int dstLen,
	 uint32_t                    dstStride )
{
	return fcvAddu8(src1, width, height, src1Stride, src2, src2Stride, FASTCV_CONVERT_POLICY_SATURATE, dst, dstStride);
}

int vx_hexagon_fcvSubtractu8_WRAP
  ( const uint8_t *             src1, int src1Len,
	uint32_t                    width,			
	uint32_t                    height,
	uint32_t                    src1Stride,
	const uint8_t * __restrict  src2, int src2Len,
	uint32_t                    src2Stride,
	uint8_t *                   dst,  int dstLen,
	uint32_t                    dstStride )
{
	return fcvSubtractu8(src1, width, height, src1Stride, src2, src2Stride, FASTCV_CONVERT_POLICY_WRAP, dst, dstStride);
}

int vx_hexagon_fcvSubtractu8_SATURATE
  ( const uint8_t *             src1, int src1Len,
	uint32_t                    width,			
	uint32_t                    height,
	uint32_t                    src1Stride,
	const uint8_t * __restrict  src2, int src2Len,
	uint32_t                    src2Stride,
	uint8_t *                   dst,  int dstLen,
	uint32_t                    dstStride )
{
	return fcvSubtractu8(src1, width, height, src1Stride, src2, src2Stride, FASTCV_CONVERT_POLICY_SATURATE, dst, dstStride);
}

int vx_hexagon_fcvFilterThresholdu8_v2
   ( const uint8_t*            src, int srcLen,
	 unsigned int              srcWidth,
	 unsigned int              srcHeight,
	 unsigned int              srcStride,
	 uint8_t*                  dst, int dstLen,
	 unsigned int              dstStride,
	 unsigned int              threshold)
{
	fcvFilterThresholdu8_v2(src, srcWidth, srcHeight, srcStride, dst, dstStride, threshold);
	//fcvFilterThresholdu8(src, srcWidth, srcHeight, dst, threshold);
	return 0;
}